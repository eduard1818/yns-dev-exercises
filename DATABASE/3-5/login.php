<?php require_once 'connection.php';

    session_start();

    if(isset($_SESSION["user_loggedin"]))	
    {
        header("location: home.php");
    }

    if(isset($_REQUEST['btn_login']))	
    {
        $username	=strip_tags($_REQUEST["username"]);	
        $password	=strip_tags($_REQUEST["password"]);			
            
        if(empty($username)){						
            $errorMsg[]="Please enter username";	
        }
        else if(empty($password)){
            $errorMsg[]="Please enter password";	
        }
        else
        {
            try
            {
                $select_stmt=$conn->prepare("SELECT * FROM users WHERE username=:uname"); 
                $select_stmt->execute(array(':uname'=>$username));	
                $row=$select_stmt->fetch(PDO::FETCH_ASSOC);
                
                if($select_stmt->rowCount() > 0)	
                {
                    if($username==$row["username"] ) 
                    {
                        if(password_verify($password, $row["password"]))
                        {
                            $_SESSION["user_loggedin"] = $row["id"];
                            echo "Success! Please wait a second...";	
                            header("refresh:2; home.php");		
                        }
                        else
                        {
                            $errorMsg[]="Invalid password";
                        }
                    }
                    else
                    {
                        $errorMsg[]="Invalid Username";
                    }
                }
                else
                {
                    $errorMsg[]="Invalid Username";
                }
            }
            catch(PDOException $e)
            {
                $e->getMessage();
            }		
        }
    }

?>


<?php require_once 'header.php';?>
<div class="jumbotron vertical-center">
        <div class="container">
            <h3>Login</h3>
            <form method = 'post'>

                <label for="user">Username:</label><br>
                <input type="text" id="username" name="username" required><br>

                <label for="pass">Password:</label><br>
                <input type="password" id="password" name="password" minlength="8" required><br><br>

                <button type="submit" name="btn_login" class="btn btn-primary">Login</button>
            </form>
            <br><br>

            <a type="button" href="index.php" class="btn btn-success">Register Here</a>
        </div>
</div>

<?php require_once 'footer.php';?>