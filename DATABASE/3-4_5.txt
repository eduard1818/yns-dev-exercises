SELECT `last_name`
FROM `employees`
WHERE `department_id` = 3
ORDER BY `last_name` DESC;