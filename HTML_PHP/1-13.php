<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-13</title>
</head>
<body>
    <?php 
        session_start();

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (login($_POST['username'], $_POST['password']) == true) {
                header('Location:1-9.php');
            }
        }

        function login($username, $password) {
            $file = fopen('usercredentials.csv', 'r');
            $tempUsername = $tempPassword = '';
            while (($list = fgetcsv($file, 0, ",")) !== false) {
                foreach($list as $lists) {
                    $tempUsername = $list[0];
                    $tempPassword = $list[1];                
                    if ($tempUsername != $username) {
                        break;
                    }   
                    elseif ($tempPassword != $password) {
                        break;
                    }
    
                    return true;
                }
    
            }
            echo 'Invalid username/password!';
            return false;
        }
    ?>

    <h3>Login Form</h3>
    <form action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method = 'post'>
        <label for="user">Username:</label><br>
        <input type="text" id="username" name="username"><br>
        <label for="pass">Password:</label><br>
        <input type="password" id="password" name="password" minlength="8" required><br><br>
        <input type="submit" value="Submit">
    </form>
    <br><br>

    <a href='1-13_register.php'>Register</a>
    
</body>
</html>