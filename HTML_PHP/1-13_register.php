<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-13_result</title>
</head>
<body>
    <?php session_start(); ?>

    <form name="myForm" action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" enctype = 'multipart/form-data'>
        <h3>Please Fill-up Form</h3>  
        <label for="uname">Username:</label>
        <input type="text" id="uname" name="uname" required><br><br>

        <label for="pword">Password:</label>
        <input type="password" id="pword" name="pword" minlength="8" required><br><br>


        <input type="submit" name="submit" value="Submit"><br><br>

    </form>

    <?php 
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (formValidation() == true) {
                
                $_SESSION['uname'] = $_POST['uname'];
                $_SESSION['pword'] = $_POST['pword'];

                header('Location:1-13_result.php');
            } 
        }

        function formValidation() {
            if (!preg_match ('/^[a-z]{4,8}[0-9]{0,4}$|^[a-z]{4,12}$/', $_POST['uname'])) {
                echo "Invalid Username!";
                return false;
            }

            if (empty($_POST["pword"]) == true) {
                echo "Input Password!";
                return false;
            }

            return true;
        }
    ?>
</body>
</html>