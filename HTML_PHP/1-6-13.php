<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-6-8</title>
    <style>
        form {
            text-align: center;
        }
    </style>
</head>
<body>
    

    <form name="myForm" action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" enctype = 'multipart/form-data'>
        <h3>Please Fill-up Form</h3>  
        <label for="fname">Full Name:</label>
        <input type="text" id="fname" name="fname" required><br><br>

        <label for="age">Age:</label>
        <input type="number" id="age" name="age" required><br><br>

        <label for="gender">Gender:</label>
        <input type="radio" name="gender" value="male" required> Male  
        <input type="radio" name="gender" value="female" required> Female <br><br>

        <label for="email">Email:</label>
        <input type="email" id="email" name="email" required><br><br>

        <label for="image">Select image:</label>
        <input type="file" id="image" name="image"><br><br>

        <input type="submit" name="submit" value="Submit"><br><br>

        <a href="http://localhost:8080/HTML_PHP/1-9.php">View CSV File Users</a>
    </form>
    
    <?php
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (formValidation() == true) {
                
                $_SESSION['fname'] = $_POST['fname'];
                $_SESSION['age'] = $_POST['age'];
                $_SESSION['gender'] = $_POST['gender'];
                $_SESSION['email'] = $_POST['email'];

                $filename    = $_FILES['image']['tmp_name'];
                $asset = 'img/' . $_FILES['image']['name']; 
                move_uploaded_file($filename, $asset);
                $_SESSION['image'] = $asset;

                header('Location:1-6-13_result.php');
            } 
        }

        function formValidation() {
            if (!preg_match ('/^[a-zA-Z\s]+$/', $_POST['fname'])) {
                echo "Invalid First Name";
                return false;
            }
  
            if (empty($_POST["age"]) == true) {
              echo "Input Age";
              return false;
            }
            
            if (!preg_match ('/^[a-zA-Z0-9\-+@.]+$/', $_POST['email'])) {
              echo "Invalid Email Address";
              return false;
            }

            $tempFile = getimagesize(($_FILES['image']['tmp_name']));        
            if ($tempFile === false)
            {
                echo 'Please select an Image.';
                return false;
            }

            return true;
        }

       
    ?>
</body>
</html>