<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-4</title>
</head>
<body>
    <form method="POST">

        <label for="input1">Enter Number:</label>
            <input type="text" id="input1" name="input1"><br><br>

        <input type="submit" name="fizzbuzz" value="Submit">

    </form>

    <?php
        if(isset($_POST['fizzbuzz']))  
        {  
            $number1 = $_POST['input1'];
            for ($i = 1; $i <= $number1; $i++)
                if ( $i%3 == 0 && $i%5 == 0 )
                {
                echo " FizzBuzz"."\n" ;
                }
                else if ( $i%3 == 0 ) 
                {
                echo " Fizz"."\n";
                }
                else if ( $i%5 == 0 ) 
                {
                echo " Buzz"."\n";
                }
                else
                {
                echo $i."\n";
                }
            
        }
       
    ?>
</body>
</html>