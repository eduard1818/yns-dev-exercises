<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-13 - Result</title>
    <style>
        img {
            width: 400px;
        }
    </style>
</head>
<body>
    <?php session_start(); ?>

    <div>
      <img src="<?php echo $_SESSION['image']; ?>" alt="picture"/>
    </div>

    <?php

        $uname = $_SESSION['uname'];
        $pword = $_SESSION['pword'];
        $hashed_password = password_hash($pword, PASSWORD_DEFAULT);

        $list = array($uname, $pword);
        $file = fopen('usercredentials.csv', 'a');
        fputcsv($file, $list);
        fclose($file);

        echo "User Credentials Added to CSV File! <br><br>";

        echo "Username: $uname <br>";
        echo "Hashed Password: $hashed_password <br>";
        
    ?>

    <a href="http://localhost/yns-dev-exercises/HTML_PHP/1-13.php">Go to Login Form</a>

        
</body>
</html>