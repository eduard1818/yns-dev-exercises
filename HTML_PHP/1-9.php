<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-9</title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        .center {
            margin-left: auto;
            margin-right: auto;
            /* margin-top: -200px; */
        }

        h3 {
            text-align: center;
        }

        a {
            text-align: center;
        }
    </style>
</head>
<body>
    
    <table class="center">
        <h3>CSV File Users</h3>
        <tr>
            <th>Full Name</th>
            <th>Age</th>
            <th>Gender</th>
            <th>Email</th>
            <th colspan="2">Image</th>
        </tr>
        <?php
            $file = fopen('users.csv', 'r');
            $column = 1;
            while (($list = fgetcsv($file, 1000, ",")) !== false) {
                echo '<tr>';
                foreach($list as $lists) {
                    if ($column % 5 == 0){
                        $image = 'img/' . $lists;
                          echo "<td> <img src='$image' height='50'> </td>";
                          $column = 1;
                          break;
                    }

                    echo "<td> $lists </td>";
                    $column++;
                }
                echo '</tr> <br>';
            }
        ?>
    </table><br><br>
    
    <div style="text-align:center">
        <a href="http://localhost/yns-dev-exercises/HTML_PHP/1-6-13.php">Back to Form</a><br><br>
        <a href="http://localhost/yns-dev-exercises/HTML_PHP/1-13_logout.php">Logout</a>
    </div>
    
</body>
</html>