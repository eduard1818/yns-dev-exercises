<?php
$host = "mysql-server";
$user = "root";
$pass = "secret";
$db = "exercise_6_1";
try {
    $conn = new PDO("mysql:host=$host;dbname=$db", $user, $pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
    //echo "DB Connected Successfully";
} catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
?>