<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-6-8 - Result</title>
    <style>
        img {
            width: 400px;
        }
    </style>
</head>
<body>
    <div class="container"> 
        <?php require_once '../navigation.php'; ?>

        <div>
        <img src="<?php echo $_SESSION['image']; ?>" alt="picture"/>
        </div>

        <?php

            $fullName = $_SESSION['fname'];
            $age = $_SESSION['age'];
            $gender = $_SESSION['gender'];
            $email = $_SESSION['email'];
            $image = substr($_SESSION['image'], 4);
            
            $list = array($fullName, $age, $gender, $email, $image);
            $file = fopen('users.csv', 'a');
            fputcsv($file, $list);
            fclose($file);

            echo "Added to CSV File! <br><br>";

            echo "Full Name: $fullName <br>";
            echo "Age: $age <br>";
            echo "Gender: $gender <br>";
            echo "Email Address: $email <br>";
            
        ?>

        <a href="http://localhost/yns-dev-exercises/HTML_PHP/1-6-13.php">Back to Form</a>
    </div>
        
</body>
</html>