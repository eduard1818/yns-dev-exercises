<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>1-5</title>
    </head>
    <body>
        <div class="container">
            <?php require_once '../navigation.php'; ?>
        
            <form method="POST" style=" margin-top: 50px;">
                <label for="InputDate">Input date:</label>
                <input type="date" name="InputDate" />
                <input type="submit" name="submitdate" value="Submit">
            </form>

            <?php

                if(isset($_POST['submitdate']))  
                {  
                    $time = strtotime($_POST['InputDate']);
                    if ($time) 
                    {
                        $new_date = date('Y-m-d', $time);
                        $date=date('d',$time);
                        $month=date('m',$time);
                        $year=date('Y',$time);
                        $days3fromnow = $date+3;
                        if($days3fromnow <= 31) {
                            echo "Three days from now is: " . $days3fromnow;
                        }
                        else
                        {
                            echo "Error Day.";
                        }
                        
                    } 
                    else 
                    {
                    echo 'Invalid Date: ' . $_POST['InputDate'];
                    // fix it.
                    }
                }

            ?>
        </body>
    </div>
</html>