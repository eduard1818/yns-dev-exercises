<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-2</title>
</head>
<body>
    <div class="container">
        <?php require_once '../navigation.php'; ?>

        <form method="POST" style=" margin-top: 50px;">

            <label for="input1">Enter Number:</label>
                <input type="text" id="input1" name="input1"><br><br>

            <label for="input2">Enter Number:</label>
                <input type="text" id="input2" name="input2"><br><br>

            <input type="submit" name="AddNumbers" value="Add Numbers">
            <input type="submit" name="SubtractNumbers" value="Subtract Numbers">
            <input type="submit" name="MultiplyNumbers" value="Multiply Numbers">
            <input type="submit" name="DivideNumbers" value="Divide Numbers">

        </form>

        <?php  
        if(isset($_POST['AddNumbers']))  
        {  
            $number1 = $_POST['input1'];  
            $number2 = $_POST['input2'];  
            $sum =  $number1+$number2;     
            echo "The sum of $number1 and $number2 is: ".$sum;   
        }  

        elseif(isset($_POST['SubtractNumbers']))
        {
            $number1 = $_POST['input1'];  
            $number2 = $_POST['input2'];  
            $difference =  $number1-$number2;     
            echo "The difference of $number1 and $number2 is: ".$difference;
        }

        elseif(isset($_POST['MultiplyNumbers']))
        {
            $number1 = $_POST['input1'];  
            $number2 = $_POST['input2'];  
            $product =  $number1*$number2;     
            echo "The product of $number1 and $number2 is: ".$product;
        }

        elseif(isset($_POST['DivideNumbers']))
        {
            $number1 = $_POST['input1'];  
            $number2 = $_POST['input2'];  
            $quotient =  $number1/$number2;     
            echo "The quotient of $number1 and $number2 is: ".$quotient;
        }
        ?>  
    </div>
    
</body>
</html>