<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-3</title>
</head>
<body>

    <div class="container">
        <?php require_once '../navigation.php'; ?>

        <form method="POST" style=" margin-top: 50px;">

            <label for="input1">Enter Number:</label>
                <input type="text" id="input1" name="input1"><br><br>

            <label for="input2">Enter Number:</label>
                <input type="text" id="input2" name="input2"><br><br>

            <input type="submit" name="findGCD" value="Find GCD">

        </form>

        <?php

            if(isset($_POST['findGCD']))  
            {  
                $number1 = $_POST['input1'];  
                $number2 = $_POST['input2'];  

                if ($number1 > $number2) {
                    $tempN = $number1;
                    $number1 = $number2;
                    $number2 = $tempN;
                    }
            
                    for($i = 1; $i < ($number1+1); $i++) {
                    if ( $number1%$i == 0 and  $number2%$i == 0)
                        $gcd = $i;
                    }
            
                    echo "GCD of $number1 and $number2 is: $gcd";
            }  
            
        ?>
    </div>
</body>
</html>