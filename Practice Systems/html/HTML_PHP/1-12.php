<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1-12</title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>
<body>
    <?php require_once '../navigation.php'; ?>
    <table>
        <h3>CSV File Users</h3>
        <tr>
            <th>Full Name</th>
            <th>Age</th>
            <th>Gender</th>
            <th>Email</th>
            <th colspan="2">Image</th>
        </tr>
        <?php
            $file = fopen('users.csv', 'r');

            $rows = 1;
            $rowsperpage = 10; 
            // find out total pages
            $totalpages = ceil($rows / $rowsperpage);
            // get the current page or set a default
            if (isset($_GET['currentpage']) && is_numeric($_GET['currentpage'])) {
                // cast var as int
                $currentpage = (int) $_GET['currentpage'];
            } else {
                // default page num
                $currentpage = 1;
            } 

            // if current page is greater than total pages...
            if ($currentpage > $totalpages) {
                // set current page to last page
                $currentpage = $totalpages;
            } // 
            // if current page is less than first page...
            if ($currentpage < 1) {
                // set current page to first page
                $currentpage = 1;
            } 

     
            $offset = ($currentpage - 1) * $rowsperpage;
            

            $row = 1; 

            $displayedRows = 0;
            while (($data = fgetcsv($file, 0, ",")) !== false) {
                if ($displayedRows == $rowsperpage) {
                    break;
                }

                if ($row >= $rowsperpage) {
                    echo '<tr>';
                    $column = 1;
                    foreach($data as $field) {
                        if ($column % 5 == 0){
                        $filename = 'img/' . $field;
                            echo "<td> <img src='$filename' height='50'> </td>";
                            $column = 1;
                            break;
                        }

                        echo "<td> $field </td>"; 
                        $column++;
                    }
                    echo '</tr>';
                    $displayedRows++;
                }

                $row++;
            }

            echo '</table>';

           
            /***pagination links ***/
            // range of num links to show
            $range = 10;

            // if not on page 1, don't show back links
            if ($currentpage > 1) {
            // show << link to go back to page 1
            echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=1'><<</a> ";
            // get previous page num
            $prevpage = $currentpage - 1;
            // show < link to go back to 1 page
            echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$prevpage'><</a> ";
            }  

            // loop to show links to range of pages around current page
            for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
            // if it's a valid page number...
            if (($x > 0) && ($x <= $totalpages)) {
                // if we're on current page...
                if ($x == $currentpage) {
                    // 'highlight' it but don't make a link
                    echo " [<b>$x</b>] ";
                // if not current page...
                } else {
                    // make it a link
                    echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$x'>$x</a> ";
                } 
            }  
            } 

            // if not on last page, show forward and last page links        
            if ($currentpage != $totalpages) {
            // get next page
            $nextpage = $currentpage + 1;
                // echo forward link for next page 
            echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$nextpage'>></a> ";
            // echo forward link for lastpage
            echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$totalpages'>>></a> ";
            }
           
        ?>
    </table><br><br>

    <a href="http://localhost/yns-dev-exercises/HTML_PHP/1-6-13.php">Back to Form</a>
</body>
</html>