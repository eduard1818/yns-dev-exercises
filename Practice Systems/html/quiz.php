<?php
require_once('connection.php');

$sql = "SELECT * FROM questions ORDER BY RAND()";
if($conn){
    $result = $conn->query($sql);
}

$data = [];
while($row = $result->fetch()){
    $data[] = $row;
}

require_once 'header.php';
?>
    <div class="container">
        <?php require_once 'navigation.php'; ?>

        <div style="margin-top: 50px;">
            <h1 style="text-align: center;">Please choose the correct answer.</h1>
            <form action="score.php" method="post">
            <?php
            foreach($data as $key=>$value){
                echo "<div class='questionItem'>";
                echo "<p>";
                echo $key + 1 . ".) " . $value['question'] . "?";
                $sql = "SELECT * FROM choices WHERE question_id='".$value['id']."' ORDER BY RAND() ";
                $choices = [];
                if($result = $conn->query($sql)){
                    while($row = $result->fetch()){
                        $choices[] = $row;
                    }
                }
                foreach($choices as $choice){
                    echo "<br> <input type='radio' name='".$value['id']."' value='".$choice['id']."'>".$choice['name'];
                }
                echo "</p>";
                echo "</div>";
            }
            ?>
            <button type="submit" class="btn btn-success">Submit</button> <br><br>
            </form>
        </div>
    </div>        

<?php require_once 'footer.php'; ?>