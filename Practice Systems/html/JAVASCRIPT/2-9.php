<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2-9</title>
</head>
<body>
    <div class="container">
        <?php require_once '../navigation.php'; ?>
    
        <div style="margin-top: 50px;">
            <button type="button" class="btn btn-primary" onclick="changeColor('blue')">Blue</button>
            <button type="button" class="btn btn-danger" onclick="changeColor('red')">Red</button>
            <button type="button" class="btn btn-success" onclick="changeColor('green')">Green</button>
            <button type="button" class="btn btn-secondary" onclick="changeColor('gray')">Gray</button>
        </div>
    </div>
    


    <script>
        function changeColor(color) {
            document.body.style.backgroundColor = color;
       
        }
    </script>
</body>
</html>