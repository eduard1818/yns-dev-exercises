<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2-4</title>
</head>
<body>
    <div class="container">
        <?php require_once '../navigation.php'; ?>

        <div style="margin-top: 50px;">
            <label for="primeN">Input Number</label><br>
            <input type="number" id="input1">
            <button onclick="findPrime()">Submit</button>

            <h4>Prime Numbers are: <p id="primeNumbers"></p></h4>
        </div>
    </div>
    

    <script>
        function findPrime(){
            var lastNum = parseInt(document.getElementById('input1').value);
            var startNum = 2;
            var i = 0;
            var primeNumbers = [];

            while(startNum <= lastNum){
                var num = 0;
                for(i = 1; i <= startNum; i++){
                    if(startNum % i == 0){
                        num++;
                    }
                }
                if(num < 3){
                    primeNumbers.push(startNum);
                }
                startNum++
            }
            document.getElementById('primeNumbers').innerHTML = primeNumbers;
        }
    </script>
</body>
</html>