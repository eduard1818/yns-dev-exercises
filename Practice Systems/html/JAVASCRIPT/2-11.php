<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2-11</title>
    <style>

    </style>
</head>
<body onclick="changebgColor();">
    <div class="container">
        <?php require_once '../navigation.php'; ?>
        <div style="margin-top: 50px;">
            <h3>Background Color Change from Dark Blue to Light Blue</h3>
        </div>
    </div>
    <script>
        document.body.style.backgroundColor = '#00008B';
        setTimeout(changebgColor, 2000);
        
        function changebgColor(){
            document.body.style.backgroundColor = '#ADD8E6';
        }
        
    </script>
</html>