<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2-8</title>
</head>
<body>
    <div class="container">
        <?php require_once '../navigation.php'; ?>

        <div style="margin-top: 50px;">
            <a type="button" class="btn btn-secondary" href='https://www.google.com/' id ="link" onclick=displayAlert()>
                Click this link.</a>
        </div>
    </div>
    
    
    <script>
        function displayAlert() {
            var link = document.getElementById('link').href;
            alert(link);
        }
    </script>
</body>
</html>