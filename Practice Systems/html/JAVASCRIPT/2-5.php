<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2-5</title>
</head>
<body>
    <div class="container">
        <?php require_once '../navigation.php'; ?>

        <div style="margin-top: 50px;">
            <input type="text" id="input1" oninput="changeLabel()"> <br>
            <h4>Result: <p for="label1" id="label1"></p></h4>
        </div>
            
    </div>
    

    <script>
        function changeLabel(){
            var result = document.getElementById('input1').value;
            document.getElementById('label1').innerHTML = result;
        }
    </script>
</body>
</html>