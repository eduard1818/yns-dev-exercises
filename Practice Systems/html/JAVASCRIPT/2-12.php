<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2-11</title>
</head>
<body>
    <div class="container">
        <?php require_once '../navigation.php'; ?>
        
        <div style="margin-top: 50px;">    
        <img > 
            <img src='img/luffy.jpg' onmouseover="this.src='img/zorro.jpg'" onmouseout="this.src='img/luffy.jpg'" width="500" height="600">
        </div>
    </div>
    <script>
        let img = document.querySelector('img');
        let start = img.src;
        let hover = img.getAttribute('data-hover'); 

        img.onmouseover = () => { img.src = hover; }
        img.onmouseout = () => { img.src = start; } 
    </script>
</body>
</html>