<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2-6</title>
</head>
<body>
    <div class="container">
        <?php require_once '../navigation.php'; ?>

        <div style="margin-top: 50px;">
            <button class="btn btn-primary" onclick="appendText()">Click Here!</button>

            <div  id='result'>
            </div>
        </div>
    </div>
   

    <script>
        function appendText(){
            var label = document.createElement('label');
            var text = document.createTextNode('Text here.');
            label.appendChild(text);        
            document.getElementById('result').appendChild(label);
            var br = document.createElement('br');
            document.getElementById('result').appendChild(br);
        }
    </script>
</body>
</html>