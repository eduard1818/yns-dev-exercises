<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2-14</title>
    <style>
        img {
            height: 500px;
        }
    </style>
</head>
<body>
    <div class="container">
        <?php require_once '../navigation.php'; ?>
        
        <div style="margin-top: 50px;">
            <label for="strawhats">Choose Image</label>
            <select name="strawhats" id="strawhats" onchange="setImage(this);" >
                <option value="img/luffy.jpg" active>Luffy</option>
                <option value="img/sanji.jpg">Sanji</option>
                <option value="img/zorro.jpg">Zorro</option>
            </select><br><br>

            <img src="" name="image-swap" /> 
        </div>
    </div>
    <script>
        function setImage(select){
            var image = document.getElementsByName("image-swap")[0];
            image.src = select.options[select.selectedIndex].value;
        }
    </script>
</body>
</html>