<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2-13</title>
    <style>
        img {
            height: 200px;
        }
    </style>
</head>
<body>
    <div class="container">
        <?php require_once '../navigation.php'; ?>

        <div style="margin-top: 50px;">
            <img src="img/luffy.jpg" alt="" id="luffy"><br><br>

            <button class="btn btn-primary" onclick="reSize('small')">Small</button>
            <button class="btn btn-secondary" onclick="reSize('medium')">Medium</button>
            <button class="btn btn-danger" onclick="reSize('large')">Large</button>
        </div>
    </div>

    <script>
        function reSize(resize){
            if(resize == 'small'){
                document.getElementById('luffy').style.height = '200px';
            } else if(resize == 'medium'){
                document.getElementById('luffy').style.height = '500px';
            } else if(resize == 'large'){
                document.getElementById('luffy').style.height = '800px';
            }
        }
    </script>
</body>
</html>