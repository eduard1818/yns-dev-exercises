
    <?php
        //session_start();
        require_once 'connection.php';

        if(isset($_REQUEST['btn_register']))
        {   
            $email		= strip_tags($_REQUEST['email']);
            $username	= strip_tags($_REQUEST['username']);	
            $password	= strip_tags($_REQUEST['password']);
            $fname	= strip_tags($_REQUEST['fname']);
            $mname	= strip_tags($_REQUEST['mname']);
            $lname	= strip_tags($_REQUEST['lname']);
            $age	= strip_tags($_REQUEST['age']);
            $dob	= strip_tags($_REQUEST['dob']);

            if(empty($email)){
                $errorMsg[]="Please enter email";	
            }
            else if(empty($username)){
                $errorMsg[]="Please enter username";
            }
            else if(empty($password)){
                $errorMsg[]="Please enter password";
            }
            else if(strlen($password) < 8){
                $errorMsg[] = "Password must be atleast 8 characters";	
            }
            else if(empty($fname)){
                $errorMsg[]="Please enter this First Name field";
            }
            else if(empty($mname)){
                $errorMsg[]="Please enter Middle Name field";
            }
            else if(empty($lname)){
                $errorMsg[]="Please enter Last Name field";
            }
            else if(empty($age)){
                $errorMsg[]="Please enter Age field";
            }
            else if(empty($dob)){
                $errorMsg[]="Please enter Date of Birth field";
            }
            else {	
                try {	


                    $filename = $_FILES['image']['name'];
                    $target_file = 'img/'.$filename;

                    $file_extension = pathinfo($target_file, PATHINFO_EXTENSION);
                    $file_extension = strtolower($file_extension);
                    $valid_extension = array("png","jpeg","jpg");

                    $hashed_password = password_hash($password, PASSWORD_DEFAULT); 
                    
                    $insert_stmt=$conn->prepare("INSERT INTO users	(email,username,password, first_name, middle_name, last_name, age,
                                                        date_of_birth, image) 
                                                VALUES  (:uemail,:uname,:upassword,:fname,:mname,:lname,:age,
                                                        :dob,:image)");

                    if(in_array($file_extension, $valid_extension)) {
                        if(move_uploaded_file( $_FILES['image']['tmp_name'],$target_file)) {					
                                
                            if($insert_stmt->execute(array(	':uemail' =>$email,':uname'	=>$username, ':upassword'=>$hashed_password,
                                                            ':fname' => $fname, ':mname' => $mname, ':lname' => $lname, ':age' => $age,
                                                            ':dob' => $dob, ':image' =>$filename  ))) {
                                                                    
                                $registerMsg="Registered Successfully..... Please Click Login Button"; 
                            
                            }
                        }
                    }
                }
                catch(PDOException $e)
                {
                    echo $e->getMessage();
                }
            }
        }
        
    ?>

    <div class="container">
    <div class="col-lg-12">
		
		<?php
            if(isset($errorMsg))
            {
                foreach($errorMsg as $error)
                {
        ?>
                    <div class="alert alert-danger">
                        <strong>WRONG ! <?php echo $error; ?></strong>
                    </div>
                <?php
                }
            }
            if(isset($registerMsg))
            {
            ?>
                <div class="alert alert-success">
                    <strong><?php echo $registerMsg; ?></strong>
                </div>
            <?php
            }
        ?>   
    </div>

<?php require_once 'header.php';?>

    <div class="container">
        <?php require_once '../navigation.php'; ?>

        <div style="margin-top: 50px;">
            
            <form method = 'post' enctype='multipart/form-data'>
                <h4>Register</h4>
                <div class="row">
                    <div class="col-md-2">
                        <label for="email">Email</label><br>
                        <input type="text" class="form-control" id="email" name="email"  required><br>
                    </div>
                    <div class="col-md-2">
                        <label for="user">Username:</label><br>
                        <input type="text" class="form-control" id="username" name="username" required><br>
                    </div>
                    <div class="col-md-2">
                        <label for="pass">Password:</label><br>
                        <input type="password" class="form-control" id="password" name="password" minlength="8" required><br><br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <label for="fname">First Name</label><br>
                        <input type="text" class="form-control" name="fname" id="fname" required><br>
                    </div>
                    <div class="col-md-2">
                        <label for="mname">Middle Name</label><br>
                        <input type="text" class="form-control" name="mname" id="mname" required><br>
                    </div>
                    <div class="col-md-2">
                        <label for="lname">Last Name:</label><br>
                        <input type="text" class="form-control" name="lname" id="lname" required><br><br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <label for="age">Age</label><br>
                        <input type="number" class="form-control" name="age" id="age" required><br>
                    </div>
                    <div class="col-md-2">
                        <label for="dob">Date of Birth:</label><br>
                        <input type="date" class="form-control" name="dob" id="dob" required><br>
                    </div>
                    <div class="col-md-2">
                        <label for="image">Upload Image:</label><br>
                        <input type="file" class="form-control" name="image" id="image" required><br>
                    </div>
                </div>

                <button type="submit" name="btn_register" class="btn btn-primary">Register</button>
            </form>
            <br><br>

            <a type="button" href="login.php" class="btn btn-success">Login Here</a>
            
        </div>
    </div>

    

<?php require_once 'footer.php';?>