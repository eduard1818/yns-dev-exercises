<?php
//session_start();
require_once 'connection.php';

        //Pagination
        $perPage = 10;
        // Calculate Total pages
        $stmt = $conn->query('SELECT count(*) FROM users');
        $total_results = $stmt->fetchColumn();
        $total_pages = ceil($total_results / $perPage);

        // Current page
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $starting_limit = ($page - 1) * $perPage;

        // Query to fetch users
        $query = "SELECT * FROM users ORDER BY id DESC LIMIT $starting_limit,$perPage";

        // Fetch all users for current page
        $users = $conn->query($query)->fetchAll();
?>

<?php require_once 'header.php';?>

<div class="container">
    <?php require_once '../navigation.php'; ?>
    
    <a type="button" href="logout.php" class="btn btn-secondary">Logout</a>

    <h1>Users</h1>

    <table class="table table-bordered table-condensed">
                <thead>
                    <tr>
                        <th>Email</th>
                        <th>Username</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>Age</th>
                        <th>Date of Birth</th>
                        <th>Profile Image</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user): ?>
                        
                        <tr>
                            <td><?php echo htmlspecialchars($user['email']) ?></td>
                            <td><?php echo htmlspecialchars($user['username']); ?></td>
                            <td><?php echo htmlspecialchars($user['first_name']); ?></td>
                            <td><?php echo htmlspecialchars($user['middle_name']); ?></td>
                            <td><?php echo htmlspecialchars($user['last_name']); ?></td>
                            <td><?php echo htmlspecialchars($user['age']); ?></td>
                            <td><?php echo htmlspecialchars($user['date_of_birth']); ?></td>
                            <td><img src="img/<?php echo htmlspecialchars($user['image']); ?>" alt="picture"/></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <?php for ($page = 1; $page <= $total_pages ; $page++):?>
                
                <a href='<?php echo "?page=$page"; ?>' class="">
                    <?php  echo "[" . $page . "]"; ?>
                </a>
        
            <?php endfor; ?>
</div>


<?php require_once 'footer.php';?>