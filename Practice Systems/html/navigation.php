<?php require_once 'header.php' ?>

<div class="container" >

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="http://localhost:8080/">YNS</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
            <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="htmlphp" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        HTML&PHP
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="htmlphp">
                        <li><a class="dropdown-item" href="http://localhost:8080/HTML_PHP/1-1.php">1-1</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/HTML_PHP/1-2.php">1-2</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/HTML_PHP/1-3.php">1-3</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/HTML_PHP/1-4.php">1-4</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/HTML_PHP/1-5.php">1-5</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/HTML_PHP/1-13.php">1-6-13</a></li>
                
                    </ul>
                </li>
                
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="js" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        JAVASCRIPT
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="htmlphp">
                        <li><a class="dropdown-item" href="http://localhost:8080/JAVASCRIPT/2-1.php">2-1</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/JAVASCRIPT/2-2.php">2-2</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/JAVASCRIPT/2-3.php">2-3</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/JAVASCRIPT/2-4.php">2-4</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/JAVASCRIPT/2-5.php">2-5</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/JAVASCRIPT/2-6.php">2-6</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/JAVASCRIPT/2-7.php">2-7</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/JAVASCRIPT/2-8.php">2-8</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/JAVASCRIPT/2-9.php">2-9</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/JAVASCRIPT/2-10.php">2-10</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/JAVASCRIPT/2-11.php">2-11</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/JAVASCRIPT/2-12.php">2-12</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/JAVASCRIPT/2-13.php">2-13</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/JAVASCRIPT/2-14.php">2-14</a></li>
                        <li><a class="dropdown-item" href="http://localhost:8080/JAVASCRIPT/2-15.php">2-15</a></li>
                
                    </ul>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="http://localhost:8080/3-5">DATABASE</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="http://localhost:8080/quiz.php">QUIZ</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="http://localhost:8080/6-2.php">CALENDAR</a>
                </li>
            </ul>
        </div>
    </div>
    </nav>

</div>



<?php require_once 'footer.php' ?>