-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql-server
-- Generation Time: Apr 11, 2022 at 03:33 AM
-- Server version: 8.0.19
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yns_exercises`
--

-- --------------------------------------------------------

--
-- Table structure for table `daily_work_shifts`
--

CREATE TABLE `daily_work_shifts` (
  `id` int NOT NULL,
  `therapist_id` int NOT NULL,
  `target_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `daily_work_shifts`
--

INSERT INTO `daily_work_shifts` (`id`, `therapist_id`, `target_date`, `start_time`, `end_time`) VALUES
(1, 1, '2022-04-07', '14:00:00', '15:00:00'),
(2, 2, '2022-04-07', '22:00:00', '23:00:00'),
(3, 3, '2022-04-07', '00:00:00', '01:00:00'),
(4, 4, '2022-04-07', '05:00:00', '05:30:00'),
(5, 1, '2022-04-07', '21:00:00', '21:45:00'),
(6, 5, '2022-04-07', '05:30:00', '05:50:00'),
(7, 3, '2022-04-07', '02:00:00', '02:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`) VALUES
(1, 'Executive'),
(2, 'Admin'),
(3, 'Sales'),
(4, 'Development'),
(5, 'Design'),
(6, 'Marketing');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `middle_name` varchar(150) DEFAULT NULL,
  `birth_date` date NOT NULL,
  `department_id` int NOT NULL,
  `hire_date` date DEFAULT NULL,
  `boss_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `last_name`, `middle_name`, `birth_date`, `department_id`, `hire_date`, `boss_id`) VALUES
(1, 'Manabu', 'Yamazaki', NULL, '1976-03-15', 1, NULL, NULL),
(2, 'Tomohiko', 'Takasago', NULL, '1974-05-24', 3, '2014-04-01', 1),
(3, 'Yuta', 'Kawakami', NULL, '1990-08-13', 4, '2014-04-01', 1),
(4, 'Shogo', 'Kubota', NULL, '1985-01-31', 4, '2014-12-01', 1),
(5, 'Lorraine ', 'San Jose', 'P.', '1983-10-11', 2, '2015-03-10', 1),
(6, 'Haille', 'Dela Cruz', 'A.', '1990-11-12', 3, '2015-02-15', 2),
(7, 'Godfrey', 'Sarmenta', 'L.', '1993-09-13', 4, '2015-01-01', 1),
(8, 'Alex', 'Amistad', 'F.', '1988-04-14', 4, '2015-04-10', 1),
(9, 'Hideshi', 'Ogoshi', NULL, '1983-07-15', 4, '2014-06-01', 1),
(10, 'Kim', '', NULL, '1977-10-16', 5, '2015-08-06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee_positions`
--

CREATE TABLE `employee_positions` (
  `id` int NOT NULL,
  `employee_id` int NOT NULL,
  `position_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `employee_positions`
--

INSERT INTO `employee_positions` (`id`, `employee_id`, `position_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 4),
(5, 2, 5),
(6, 4, 5),
(7, 5, 5),
(8, 6, 5),
(9, 7, 5),
(10, 8, 5),
(11, 9, 5),
(12, 10, 5);

-- --------------------------------------------------------

--
-- Table structure for table `master_data`
--

CREATE TABLE `master_data` (
  `id` int DEFAULT NULL,
  `parent_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `master_data`
--

INSERT INTO `master_data` (`id`, `parent_id`) VALUES
(1, NULL),
(2, 5),
(3, NULL),
(4, 1),
(5, NULL),
(6, 3),
(7, 3);

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` int NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`) VALUES
(1, 'CEO'),
(2, 'CTO'),
(3, 'CFO'),
(4, 'Manager'),
(5, 'Staff');

-- --------------------------------------------------------

--
-- Table structure for table `therapists`
--

CREATE TABLE `therapists` (
  `id` int NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `therapists`
--

INSERT INTO `therapists` (`id`, `name`) VALUES
(1, 'John'),
(2, 'Arnold'),
(3, 'Robert'),
(4, 'Ervin'),
(5, 'Smith');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `email` varchar(150) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `middle_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `age` int NOT NULL,
  `date_of_birth` date NOT NULL,
  `image` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `first_name`, `middle_name`, `last_name`, `age`, `date_of_birth`, `image`) VALUES
(6, 'luffy@gmail.com', 'luffy8', '$2y$10$n6/Y5GpSH4eJdPaTLJzPoeYpQCx9IhuHnIsMi1n.jhMNQrAKazkr6', 'luffy', 'd', 'monkey', 25, '1997-10-17', 'luffy.jpg'),
(7, 'ed@gmail.com', 'eduardo8', '$2y$10$i1qduHJJ46leTGZ3fDsIlOGfrnlmir9DYelJ.6flsDhHQ1vqVl.dq', 'eduardo', 'dano', 'tumimbo', 24, '1997-10-17', '1.jpg'),
(8, 'zorro@gmail.com', 'zorro8', '$2y$10$.52ylqHxd./g3f.WbPgAsu7NzCQwZ2I3uJlfNe52psBuGnCtlBBei', 'Zorro', 'd', 'zororo', 25, '1995-08-08', 'zorro.jpg'),
(9, 'ed@gmail.com', 'eduardo1', '$2y$10$KMGKQp7iV1mPEUviei0ndOVN4wPrtnR839Fcud9NCYtfAw4QvPJCu', 'eduardo', 'dano', 'tumimbo', 24, '1997-10-17', '1.jpg'),
(10, 'dasd@gmail.com', 'qwerty123', '$2y$10$F6//h5kaZNepWoU4.yqgR.7QymhK2cfWeHofzRNj2Wr.f1VxQnHB6', 'qwerty', 'qwerty', 'qwerty', 24, '1997-10-17', 'luffy.jpg'),
(11, 'sanji@gmail.com', 'sanji8', '$2y$10$THVSGPQFidDjpFnRhPD1WOsWoCbjt1MYdNsUWCzNZ2kDGjlAtL2lq', 'sanji', 'd', 'vinsmoke', 25, '1995-10-10', 'sanji.jpg'),
(12, 'dasd@gmail.com', 'asdasd', '$2y$10$wD0RGmHQPO1XxTEfYt151OHrNZdZTVkaiECYdch5aqZRhqPAr8cdO', 'asdasd', 'asdasd', 'asdasd', 24, '1990-10-10', 'zorro.jpg'),
(13, 'ed@gmail.com', 'eduardo8', '$2y$10$ZuiSYK4YHQ0xulezSwTNOuhY4DaEsuo0JXthXDBNMIhTHTkPlHeYa', 'eduardo', 'dano', 'tumimbo', 24, '1997-10-17', '1.jpg'),
(14, 'luffy@gmail.com', 'luffy8', '$2y$10$D8vcaJ4Fbr8hJiLaS3R41OkgDZLR0Vz6wxt0Ujqi0OubUPayZP812', 'luffy', 'd', 'monkey', 24, '1992-12-19', 'luffy.jpg'),
(15, 'zorro@gmail.com', 'zorro8', '$2y$10$4CI.5IGzv/uKlo/TlsAUJerSxqQL1nM8tw5o30M6wbhPh47omLCI.', 'Zorro', 'd', 'zororo', 25, '1995-10-10', 'zorro.jpg'),
(16, 'sanji@gmail.com', 'sanji8', '$2y$10$oYw2/RJmGXC5KBHMSl2.peXOFP2dXF/l4bNJV3sjTC3iG9FpBZkt6', 'sanji', 'd', 'vinsmoke', 25, '1990-10-10', 'sanji.jpg'),
(17, 'zorro@gmail.com', 'zorro8', '$2y$10$aaFmNqQYh6UTdEVPfn3v1eLbcngLiAOE77qRT1W4lf5VISzAhNnAe', 'Zorro', 'd', 'zororo', 24, '1995-10-10', 'zorro.jpg'),
(18, 'chopper@gmail.com', 'chopper8', '$2y$10$Me1/.dsFQsPV9yQ4tG8UN.Tq6ALJzigzHiXpJa80ckM2jvOnj1JKu', 'Chopper', 'D', 'Tonytony', 21, '1998-10-17', 'chopper.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daily_work_shifts`
--
ALTER TABLE `daily_work_shifts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `therapist_id` (`therapist_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `employee_positions`
--
ALTER TABLE `employee_positions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `position_id` (`position_id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `therapists`
--
ALTER TABLE `therapists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daily_work_shifts`
--
ALTER TABLE `daily_work_shifts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `employee_positions`
--
ALTER TABLE `employee_positions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `therapists`
--
ALTER TABLE `therapists`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `daily_work_shifts`
--
ALTER TABLE `daily_work_shifts`
  ADD CONSTRAINT `daily_work_shifts_ibfk_1` FOREIGN KEY (`therapist_id`) REFERENCES `therapists` (`id`);

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`);

--
-- Constraints for table `employee_positions`
--
ALTER TABLE `employee_positions`
  ADD CONSTRAINT `employee_positions_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`),
  ADD CONSTRAINT `employee_positions_ibfk_2` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
